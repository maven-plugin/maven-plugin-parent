package com.atoken;

import static org.junit.Assert.assertTrue;

import com.alibaba.fastjson.JSON;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() throws ParseException {
        String temp = "2019-06-04T13:07:58.948925156Z".substring(0, "2019-06-04T13:07:58.948925156Z".indexOf('.')) + "Z";
        System.out.println("temp = " + temp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSS'Z'");
        Date date = simpleDateFormat.parse("2019-06-04T13:07:58.948925156Z");
        System.out.println("date.getTime() = " + date.getTime());
        System.out.println("new Date().getTime() = " + new Date().getTime());
    }

    @Test
    public void testTime() {
        String temp = readFileStr("imtoken.txt");
        List<ValidatorYearYield> validatorYearYieldLsit = JSON.parseArray(temp, ValidatorYearYield.class);
        System.out.println("validatorYearYieldLsit = " + validatorYearYieldLsit);
    }


    private static String readFileStr(String fileName) {
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(fileName));
            StringBuilder entityBuilder = new StringBuilder();
            String tmpStr;
            while((tmpStr = bufferedReader.readLine()) != null) {
                entityBuilder.append(tmpStr);
                entityBuilder.append("\n");
            }
            return entityBuilder.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (Objects.nonNull(bufferedReader)) {
                try {
                    bufferedReader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class ValidatorYearYield {
        private String operatorAddress;
        private Double annualizedReturns;

        public String getOperatorAddress() {
            return this.operatorAddress;
        }

        public void setOperatorAddress(String operatorAddress) {
            this.operatorAddress = operatorAddress;
        }

        public Double getAnnualizedReturns() {
            return this.annualizedReturns;
        }

        public void setAnnualizedReturns(Double annualizedReturns) {
            this.annualizedReturns = annualizedReturns;
        }
    }
}
