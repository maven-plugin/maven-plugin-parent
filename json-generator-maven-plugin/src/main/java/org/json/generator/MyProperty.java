package org.json.generator;

public class MyProperty {
    private String jsonPropertyName;
    private String entityPropertyName;
    private String propertyType;

    public String getJsonPropertyName() {
        return jsonPropertyName;
    }

    public void setJsonPropertyName(String jsonPropertyName) {
        this.jsonPropertyName = jsonPropertyName;
    }

    public String getEntityPropertyName() {
        return entityPropertyName;
    }

    public void setEntityPropertyName(String entityPropertyName) {
        this.entityPropertyName = entityPropertyName;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }
}
