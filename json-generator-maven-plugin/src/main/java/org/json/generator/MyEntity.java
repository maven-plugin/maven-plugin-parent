package org.json.generator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MyEntity {
    private String jsonName;
    private String entityName;
    private List<MyProperty> properties;
    private Map<String, MyProperty> propertyMap = new HashMap<>();

    public String getJsonName() {
        return jsonName;
    }

    public void setJsonName(String jsonName) {
        this.jsonName = jsonName;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public List<MyProperty> getProperties() {
        return properties;
    }

    public void setProperties(List<MyProperty> properties) {
        this.properties = properties;
        if(Objects.nonNull(properties)) {
            for(MyProperty property: properties) {
                this.propertyMap.put(property.getJsonPropertyName(), property);
            }
        }
    }

    public MyProperty getProperty(String jsonPropertyName) {
        return this.propertyMap.get(jsonPropertyName);
    }

    public boolean contain(String jsonPropertyName) {
        return this.propertyMap.containsKey(jsonPropertyName);
    }
}
