package org.json.generator;

import java.util.stream.Stream;

public class StringUtil {
    public static String nameToHumpName(String name) {
        String[] nameSplits = name.split("_");
        StringBuilder result = new StringBuilder();
        for(String nameSplit: nameSplits) {
            result.append(toUpperCaseFirstOne(nameSplit));
        }
        return result.toString();
    }

    public static String toLowerCaseFirstOne(String s){
        if(Character.isLowerCase(s.charAt(0)))
            return s;
        else
            return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
    }

    public static String toUpperCaseFirstOne(String s){
        if(Character.isUpperCase(s.charAt(0)))
            return s;
        else
            return (new StringBuilder()).append(Character.toUpperCase(s.charAt(0))).append(s.substring(1)).toString();
    }
}

