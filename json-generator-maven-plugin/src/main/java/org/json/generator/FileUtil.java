package org.json.generator;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class FileUtil {
    public static String readFile(URL url) {
        BufferedReader entityReader = null;
        try {
            entityReader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuilder entityBuilder = new StringBuilder();
            String tempStr;
            while((tempStr = entityReader.readLine()) != null) {
                entityBuilder.append(tempStr);
                entityBuilder.append("\n");
            }
            return entityBuilder.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if(Objects.nonNull(entityReader)) {
                try {
                    entityReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void writeFile(String content, String path, String fileName, boolean overwrite) {
        File file = new File(path);
        if(!file.exists()) {
            file.mkdirs();
        }
        Path filePath = Paths.get(path + fileName + ".java");
        try (BufferedWriter writer = Files.newBufferedWriter(filePath))
        {
            writer.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String packageToPath(String packageName) {
        String[] packageNames = packageName.split("\\.");
        StringBuilder result = new StringBuilder();
        for(String packageN: packageNames) {
            result.append(packageN).append("/");
        }
        return result.toString();
    }
}
