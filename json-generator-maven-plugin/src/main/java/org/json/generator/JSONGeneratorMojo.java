package org.json.generator;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;


/**
 * Goal which touches a timestamp file.
 *
 * @param generatorConfigPath
 * @goal touch
 * @phase process-sources
 */
@Mojo(name = "jsonGenerator", defaultPhase = LifecyclePhase.PROCESS_CLASSES)
public class JSONGeneratorMojo extends AbstractMojo {
    @Parameter(property = "generatorConfigPath", defaultValue = "${project.basedir}/src/main/resources/generatorConfig.json")
    private String generatorConfigPath;

    @Parameter(property = "targetPackage", defaultValue = "")
    private String targetPackage;

    @Parameter(property = "targetPath", defaultValue = "${project.basedir}/src/main/java/")
    private String targetPath;

    @Parameter(property = "jsonPath", defaultValue = "${project.basedir}/src/main/resources/json/")
    private String jsonPath;

    @Parameter(property = "allFile", defaultValue = "false")
    private boolean allFile;

    @Parameter(property = "overwrite", defaultValue = "false")
    private boolean overwrite;

    private String projectPath = System.getProperty("user.dir");
    private String classPath = getClass().getProtectionDomain().getCodeSource().getLocation().getFile();

    private String entityTemp = null;
    private String propertyTemp = null;
    private Map<String, String> importMap = new HashMap<>();

    {
        try {
            URL entityUrl = new URL("jar:file:" + classPath + "!/Entity.temp");
            URL propertyUrl = new URL("jar:file:" + classPath + "!/PropertyInfo.temp");
            entityTemp = FileUtil.readFile(entityUrl);
            propertyTemp = FileUtil.readFile(propertyUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        importMap.put("BigDecimal", "import java.math.BigDecimal;\n");
        importMap.put("List<BigDecimal>", "import java.math.BigDecimal;\n");
        importMap.put("List","import java.util.List;\n");
        importMap.put("Date", "import java.util.Date;\n");
    }

    public void execute() {
        try {
            File generatorConfigFile = new File(generatorConfigPath);
            GeneratorConfig generatorConfig;
            if (generatorConfigFile.exists()) {
                String generatorConfigStr = FileUtil.readFile(generatorConfigFile.toURI().toURL());
                generatorConfig = JSON.parseObject(generatorConfigStr, GeneratorConfig.class);
            } else {
                generatorConfig = new GeneratorConfig();
                generatorConfig.setJsonPath(jsonPath);
                generatorConfig.setTargetPackage(targetPackage);
                generatorConfig.setTargetPath(targetPath);
                generatorConfig.setAllFile(allFile);
                generatorConfig.setOverwrite(overwrite);
            }

            File jsonFilePath = new File(generatorConfig.getJsonPath());
            if (jsonFilePath.exists() && jsonFilePath.isDirectory()) {
                for (File jsonFile : jsonFilePath.listFiles()) {
                    if (".json".equals(jsonFile.getName().substring(jsonFile.getName().length() - 5))) {
                        String fileName = jsonFile.getName().substring(0, jsonFile.getName().length() - 5);
                        if (generatorConfig.isAllFile()) {
                            generatorHandler(jsonFile, generatorConfig, fileName);
                        } else {
                            if(generatorConfig.containEntity(fileName)) {
                                generatorHandler(jsonFile, generatorConfig, fileName);
                            }
                        }
                    }
                }
            } else {
                getLog().error(generatorConfig.getJsonPath() + " is not exists!");
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        getLog().info(projectPath);
    }

    private void generatorHandler(File jsonFile,GeneratorConfig generatorConfig, String fileName) throws MalformedURLException {
        String jsonStr = FileUtil.readFile(jsonFile.toURI().toURL());
        if (jsonStr.endsWith("]")) {
            JSONArray jsonArray = JSON.parseArray(jsonStr);
            this.generatorJavaFile(jsonArray.getJSONObject(0), generatorConfig, generatorConfig.getEntity(fileName), Objects.nonNull(generatorConfig.getEntity(fileName)) && StringUtils.isNotEmpty(generatorConfig.getEntity(fileName).getEntityName()) ? generatorConfig.getEntity(fileName).getEntityName() : StringUtil.nameToHumpName(fileName));
        } else {
            JSONObject jsonObject = JSON.parseObject(jsonStr);
            this.generatorJavaFile(jsonObject, generatorConfig, generatorConfig.getEntity(fileName), Objects.nonNull(generatorConfig.getEntity(fileName)) && StringUtils.isNotEmpty(generatorConfig.getEntity(fileName).getEntityName()) ? generatorConfig.getEntity(fileName).getEntityName() : StringUtil.nameToHumpName(fileName));
        }
    }

    private void generatorJavaFile(JSONObject object, GeneratorConfig generatorConfig, MyEntity entity, String fileName) {
        Set<String> importSet = new HashSet<>();
        List<MyProperty> propertyList = new ArrayList<>();
        for (Map.Entry<String, Object> entry : object.entrySet()) {
            MyProperty property = new MyProperty();
            MyProperty propertyConfig = null;
            if (Objects.nonNull(entity)) {
                propertyConfig = entity.getProperty(entry.getKey());
            }
            property.setEntityPropertyName(Objects.nonNull(propertyConfig) && StringUtils.isNotEmpty(propertyConfig.getEntityPropertyName()) ? propertyConfig.getEntityPropertyName() : StringUtil.toLowerCaseFirstOne(StringUtil.nameToHumpName(entry.getKey())));
            if (entry.getValue() instanceof JSONObject) {
                property.setPropertyType(Objects.nonNull(propertyConfig) && StringUtils.isNoneBlank(propertyConfig.getPropertyType()) ? propertyConfig.getPropertyType() : StringUtil.nameToHumpName(entry.getKey()));
                this.generatorJavaFile((JSONObject) entry.getValue(), generatorConfig, entity, property.getPropertyType());
            } else if (entry.getValue() instanceof JSONArray) {
                importSet.add(importMap.get("List"));
                if(((JSONArray) entry.getValue()).isEmpty()){
                    property.setPropertyType(Objects.nonNull(propertyConfig) && StringUtils.isNoneBlank(propertyConfig.getPropertyType()) ? propertyConfig.getPropertyType() : "List<String>");
                } else if (((JSONArray) entry.getValue()).get(0) instanceof JSONObject) {
                    property.setPropertyType(Objects.nonNull(propertyConfig) && StringUtils.isNoneBlank(propertyConfig.getPropertyType()) ? propertyConfig.getPropertyType() : "List<" + StringUtil.nameToHumpName(entry.getKey()) + ">");
                    this.generatorJavaFile((JSONObject) ((JSONArray) entry.getValue()).get(0), generatorConfig, entity, property.getPropertyType().replace("List<", "").replace(">", ""));
                } else {
                    property.setPropertyType(Objects.nonNull(propertyConfig) && StringUtils.isNoneBlank(propertyConfig.getPropertyType()) ? propertyConfig.getPropertyType() : "List<" + ((JSONArray) entry.getValue()).get(0).getClass().getSimpleName() + ">");
                    importSet.add(importMap.get(property.getPropertyType()));
                }
            } else {
                property.setPropertyType(Objects.nonNull(propertyConfig) && StringUtils.isNoneBlank(propertyConfig.getPropertyType()) ? propertyConfig.getPropertyType() : Objects.nonNull(entry.getValue()) ? entry.getValue().getClass().getSimpleName() : "String");
                importSet.add(importMap.get(property.getPropertyType()));
            }
            propertyList.add(property);
        }
        getLog().info("propertyList:" + JSON.toJSONString(propertyList));

        String propertyResult = "{propertyInfo}";
        StringBuilder importBuilder = new StringBuilder("");
        for(String importStr: importSet) {
            if(StringUtils.isNotEmpty(importStr)) {
                importBuilder.append(importStr);
            }
        }
        for (MyProperty property : propertyList) {
            String temp = propertyTemp.replace("{protertyType}", property.getPropertyType())
                    .replace("{propertyName}", property.getEntityPropertyName())
                    .replace("{PropertyName}", StringUtil.nameToHumpName(property.getEntityPropertyName()));
            propertyResult = propertyResult.replace("{propertyInfo}", temp);
        }
        propertyResult = propertyResult.replace("{propertyInfo}", "");

        String entityReult = entityTemp.replace("{targetPackage}", generatorConfig.getTargetPackage())
                .replace("{entityName}", fileName)
                .replace("{propertyInfos}", propertyResult)
                .replace("{imports}", importBuilder.toString());
        getLog().info("entityReult:\n" + entityReult);

        FileUtil.writeFile(entityReult, generatorConfig.getTargetPath() + FileUtil.packageToPath(generatorConfig.getTargetPackage()), fileName, false);
    }
}
