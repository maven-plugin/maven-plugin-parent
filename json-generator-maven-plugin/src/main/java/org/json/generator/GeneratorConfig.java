package org.json.generator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class GeneratorConfig {
    private String targetPackage = "";
    private String targetPath = "src/main/java/";
    private String jsonPath = "src/main/resources/json/";
    private boolean allFile = true;
    private boolean overwrite = false;
    private String projectPath = System.getProperty("user.dir") + "/";
    private List<MyEntity> jsonEntities;
    private Map<String, MyEntity> entityMap = new HashMap<>();

    public String getProjectPath() {
        return projectPath;
    }

    public void setProjectPath(String projectPath) {
        this.projectPath = projectPath;
    }

    public List<MyEntity> getJsonEntities() {
        return jsonEntities;
    }

    public void setJsonEntities(List<MyEntity> jsonEntities) {
        this.jsonEntities = jsonEntities;
        if(Objects.nonNull(jsonEntities)) {
            for(MyEntity jsonEntity: jsonEntities) {
                this.entityMap.put(jsonEntity.getJsonName(), jsonEntity);
            }
        }
    }

    public boolean containEntity(String fileName) {
        return entityMap.containsKey(fileName);
    }

    public MyEntity getEntity(String jsonName) {
        return entityMap.get(jsonName);
    }


    public String getTargetPackage() {
        return targetPackage;
    }

    public void setTargetPackage(String targetPackage) {
        this.targetPackage = targetPackage;
    }

    public String getTargetPath() {
        return targetPath;
    }

    public void setTargetPath(String targetPath) {
        this.targetPath = targetPath.substring(0, 1).equals("/") ? targetPath : projectPath + targetPath;
        ;
    }

    public String getJsonPath() {
        return jsonPath;
    }

    public void setJsonPath(String jsonPath) {
        this.jsonPath = jsonPath.substring(0, 1).equals("/") ? jsonPath : projectPath + jsonPath;
    }

    public boolean isAllFile() {
        return allFile;
    }

    public void setAllFile(boolean allFile) {
        this.allFile = allFile;
    }

    public boolean isOverwrite() {
        return overwrite;
    }

    public void setOverwrite(boolean overwrite) {
        this.overwrite = overwrite;
    }
}
