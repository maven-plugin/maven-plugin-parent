package com.yun.test;


/**
 * @author susky
 * @version V1.0
 * @Title: Payload
 * @Package com.yun.test
 * @Description:
 * @date 2019/2/13 3:45 PM
 */
public class Payload {
    private String code;


    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

}
