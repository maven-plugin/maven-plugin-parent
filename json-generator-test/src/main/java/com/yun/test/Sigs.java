package com.yun.test;

import java.util.List;

/**
 * @author susky
 * @version V1.0
 * @Title: Sigs
 * @Package com.yun.test
 * @Description:
 * @date 2019/2/13 3:45 PM
 */
public class Sigs {
    private List<String> pubKeys;

    private List<String> sigData;

    private Integer m;


    public void setM(Integer m) {
        this.m = m;
    }

    public Integer getM() {
        return this.m;
    }

    public void setSigData(List<String> sigData) {
        this.sigData = sigData;
    }

    public List<String> getSigData() {
        return this.sigData;
    }

    public void setPubKeys(List<String> pubKeys) {
        this.pubKeys = pubKeys;
    }

    public List<String> getPubKeys() {
        return this.pubKeys;
    }

}
