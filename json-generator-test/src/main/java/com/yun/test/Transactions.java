package com.yun.test;

import java.util.List;

/**
 * @author susky
 * @version V1.0
 * @Title: Transactions
 * @Package com.yun.test
 * @Description:
 * @date 2019/2/13 3:45 PM
 */
public class Transactions {
    private Integer nonce;

    private String payer;

    private Integer txType;

    private Integer version;

    private List<Sigs> sigs;

    private List<String> attributes;

    private Integer gasPrice;

    private Payload payload;

    private Integer height;

    private String hash;

    private Integer gasLimit;


    public void setGasLimit(Integer gasLimit) {
        this.gasLimit = gasLimit;
    }

    public Integer getGasLimit() {
        return this.gasLimit;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getHash() {
        return this.hash;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getHeight() {
        return this.height;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public Payload getPayload() {
        return this.payload;
    }

    public void setGasPrice(Integer gasPrice) {
        this.gasPrice = gasPrice;
    }

    public Integer getGasPrice() {
        return this.gasPrice;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }

    public List<String> getAttributes() {
        return this.attributes;
    }

    public void setSigs(List<Sigs> sigs) {
        this.sigs = sigs;
    }

    public List<Sigs> getSigs() {
        return this.sigs;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getVersion() {
        return this.version;
    }

    public void setTxType(Integer txType) {
        this.txType = txType;
    }

    public Integer getTxType() {
        return this.txType;
    }

    public void setPayer(String payer) {
        this.payer = payer;
    }

    public String getPayer() {
        return this.payer;
    }

    public void setNonce(Integer nonce) {
        this.nonce = nonce;
    }

    public Integer getNonce() {
        return this.nonce;
    }

}
