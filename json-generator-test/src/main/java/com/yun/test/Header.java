package com.yun.test;

import java.util.List;

/**
 * @author susky
 * @version V1.0
 * @Title: Header
 * @Package com.yun.test
 * @Description:
 * @date 2019/2/13 3:45 PM
 */
public class Header {
    private String transactionsRoot;

    private String consensusPayload;

    private Integer version;

    private List<String> sigData;

    private String prevBlockHash;

    private String nextBookkeeper;

    private List<String> bookkeepers;

    private Integer height;

    private Long consensusData;

    private String hash;

    private Integer timestamp;

    private String blockRoot;


    public void setBlockRoot(String blockRoot) {
        this.blockRoot = blockRoot;
    }

    public String getBlockRoot() {
        return this.blockRoot;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getTimestamp() {
        return this.timestamp;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getHash() {
        return this.hash;
    }

    public void setConsensusData(Long consensusData) {
        this.consensusData = consensusData;
    }

    public Long getConsensusData() {
        return this.consensusData;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getHeight() {
        return this.height;
    }

    public void setBookkeepers(List<String> bookkeepers) {
        this.bookkeepers = bookkeepers;
    }

    public List<String> getBookkeepers() {
        return this.bookkeepers;
    }

    public void setNextBookkeeper(String nextBookkeeper) {
        this.nextBookkeeper = nextBookkeeper;
    }

    public String getNextBookkeeper() {
        return this.nextBookkeeper;
    }

    public void setPrevBlockHash(String prevBlockHash) {
        this.prevBlockHash = prevBlockHash;
    }

    public String getPrevBlockHash() {
        return this.prevBlockHash;
    }

    public void setSigData(List<String> sigData) {
        this.sigData = sigData;
    }

    public List<String> getSigData() {
        return this.sigData;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getVersion() {
        return this.version;
    }

    public void setConsensusPayload(String consensusPayload) {
        this.consensusPayload = consensusPayload;
    }

    public String getConsensusPayload() {
        return this.consensusPayload;
    }

    public void setTransactionsRoot(String transactionsRoot) {
        this.transactionsRoot = transactionsRoot;
    }

    public String getTransactionsRoot() {
        return this.transactionsRoot;
    }

}
