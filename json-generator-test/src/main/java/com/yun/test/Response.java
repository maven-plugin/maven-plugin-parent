package com.yun.test;

import java.util.List;

/**
 * @author susky
 * @version V1.0
 * @Title: Response
 * @Package com.yun.test
 * @Description:
 * @date 2019/2/13 3:45 PM
 */
public class Response {
    private List<Transactions> transactions;

    private Header header;

    private Integer size;

    private String hash;


    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getHash() {
        return this.hash;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getSize() {
        return this.size;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Header getHeader() {
        return this.header;
    }

    public void setTransactions(List<Transactions> transactions) {
        this.transactions = transactions;
    }

    public List<Transactions> getTransactions() {
        return this.transactions;
    }

}
