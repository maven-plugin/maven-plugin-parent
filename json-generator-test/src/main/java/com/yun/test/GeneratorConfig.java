package com.yun.test;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author susky
 * @version V1.0
 * @Title: GeneratorConfig
 * @Package com.yun.test
 * @Description:
 * @date 2019/2/13 3:45 PM
 */
public class GeneratorConfig {
    private List<Entity> jsonEntities;

    private String targetPath;

    private String jsonPath;

    private String targetPackage;

    private BigDecimal nonce;

    private Integer age;


    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAge() {
        return this.age;
    }

    public void setNonce(BigDecimal nonce) {
        this.nonce = nonce;
    }

    public BigDecimal getNonce() {
        return this.nonce;
    }

    public void setTargetPackage(String targetPackage) {
        this.targetPackage = targetPackage;
    }

    public String getTargetPackage() {
        return this.targetPackage;
    }

    public void setJsonPath(String jsonPath) {
        this.jsonPath = jsonPath;
    }

    public String getJsonPath() {
        return this.jsonPath;
    }

    public void setTargetPath(String targetPath) {
        this.targetPath = targetPath;
    }

    public String getTargetPath() {
        return this.targetPath;
    }

    public void setJsonEntities(List<Entity> jsonEntities) {
        this.jsonEntities = jsonEntities;
    }

    public List<Entity> getJsonEntities() {
        return this.jsonEntities;
    }

}
