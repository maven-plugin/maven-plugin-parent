package com.yun.test;


/**
 * @author susky
 * @version V1.0
 * @Title: Properties
 * @Package com.yun.test
 * @Description:
 * @date 2019/2/13 3:45 PM
 */
public class Properties {
    private String entityPropertyName;

    private String jsonPropertyName;

    private String propertyType;


    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getPropertyType() {
        return this.propertyType;
    }

    public void setJsonPropertyName(String jsonPropertyName) {
        this.jsonPropertyName = jsonPropertyName;
    }

    public String getJsonPropertyName() {
        return this.jsonPropertyName;
    }

    public void setEntityPropertyName(String entityPropertyName) {
        this.entityPropertyName = entityPropertyName;
    }

    public String getEntityPropertyName() {
        return this.entityPropertyName;
    }

}
