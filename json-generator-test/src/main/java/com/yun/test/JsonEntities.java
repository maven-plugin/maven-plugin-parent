package com.yun.test;

import java.util.List;

/**
 * @author susky
 * @version V1.0
 * @Title: JsonEntities
 * @Package com.yun.test
 * @Description:
 * @date 2019/2/13 3:45 PM
 */
public class JsonEntities {
    private String jsonName;

    private String entityName;

    private List<Properties> properties;


    public void setProperties(List<Properties> properties) {
        this.properties = properties;
    }

    public List<Properties> getProperties() {
        return this.properties;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityName() {
        return this.entityName;
    }

    public void setJsonName(String jsonName) {
        this.jsonName = jsonName;
    }

    public String getJsonName() {
        return this.jsonName;
    }

}
